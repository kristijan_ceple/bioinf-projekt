 import java.util.*;

public class ProfileHMM {

	public class LDCounter {
		private int letterCount, dashCount;
		private Map<Character, Integer> letterFreqs;

		public LDCounter() {
			this.clear();
		}

		/**
		 * Count by column
		 * @param currCol current column
		 */
		public void countAtIndex(int currCol) {
			// Prebrojavanje znakova u trenutnom stupcu - pocetan stupac je 0
			char currChar;
			for(int currentRow = 0; currentRow < ProfileHMM.this.numOfRows; currentRow++) {
				currChar = ProfileHMM.this.MSA.get(currentRow).get(currCol);
				if (currChar != '-') {
					letterCount++;
					this.letterFreqs.merge(currChar, 1, (alt, neu) -> alt+1 );
				} else {
					dashCount++;
				}
			}
		}

		/**
		 * Count by column and specific rows
		 *
		 * @param currCol Current column
		 * @param rows Current rows
		 */
		public void countAtIndex(int currCol, int... rows) {
			char currChar;
			for(int currRow : rows) {
				currChar = ProfileHMM.this.MSA.get(currRow).get(currCol);
				if (currChar != '-') {
					letterCount++;
					this.letterFreqs.merge(currChar, 1, (alt, neu) -> alt+1 );
				} else {
					dashCount++;
				}
			}
		}

		public void clearAndCountAtIndex(int currCol) {
			this.clear();
			this.countAtIndex(currCol);
		}

		public void clearAndCountAtIndex(int currCol, int...rows) {
			this.clear();
			this.countAtIndex(currCol, rows);
		}

		public void clear() {
			this.letterCount = 0;
			this.dashCount = 0;
			this.letterFreqs = new HashMap<>(ProfileHMM.this.lettersFreqBlueprint);
		}

		public void addLetter(Character currChar) {
			this.letterCount++;
			this.letterFreqs.merge(currChar, 1, (alt, neu) -> alt+1);
		}
	}

	private static int idCounter = 0;
	public final int id;

	int numOfCols;
	int numOfRows;
	LinkedList<LinkedList<Character>> MSA;

	List<Integer> conservedRegions = new ArrayList<>();
	Set<Character> charactersSet = new HashSet<>();
	Map<Character, Integer> lettersFreqBlueprint = new HashMap<>();
	Map<String, Map<String,Double>> transitionsMap = new HashMap<>();
	Map<String, Map<Character, Double>> emissionsMap = new HashMap<>();

	final double THRESHOLD;
	final double PSEUDOCOUNT_EMISSION;
	final double PSEUDOCOUNT_TRANSITION;
	final double PSEUDOCOUNT_EMISSION_DENOMINATOR;
	final double PSEUDOCOUNT_TRANSITION_DENOMINATOR;
	final double HMM_OUTGOING_TRANSITIONS = 3;			// Ne vredi za End i stupac stanja prije njega

	public ProfileHMM(LinkedList<LinkedList<Character>> MSA, double PSEUDOCOUNT_EMISSION, double PSEUDOCOUNT_TRANSITION
			, double THRESHOLD) {
		this.id = ProfileHMM.idCounter++;
		this.numOfCols = MSA.get(0).size();
		this.numOfRows = MSA.size();
		this.PSEUDOCOUNT_EMISSION = PSEUDOCOUNT_EMISSION;
		this.PSEUDOCOUNT_TRANSITION = PSEUDOCOUNT_TRANSITION;
		this.THRESHOLD = THRESHOLD;
		this.MSA = MSA;
		this.updateHMMProfile();
		this.PSEUDOCOUNT_EMISSION_DENOMINATOR = (this.charactersSet.size()-1) * this.PSEUDOCOUNT_EMISSION;
		this.PSEUDOCOUNT_TRANSITION_DENOMINATOR = this.HMM_OUTGOING_TRANSITIONS * this.PSEUDOCOUNT_EMISSION;
	}

	/**
	 * Default values constructor.
	 * Emissions Pseudocount: 0.0
	 * Transition Pseudocount: 0.0
	 * Threshold: 0.7
	 *
	 * @param MSA MSA which will be profiled
	 */
	public ProfileHMM(LinkedList<LinkedList<Character>> MSA) {
		this(MSA, 0.0, 0.0, 0.7);
	}

	public void makeCharsSet() {
		char currChar = '-';
		for (int i = 0; i < this.numOfRows; i++) {
			int colLen = this.MSA.get(i).size();
			// Napraviti set svih karaktera
			for(int j = 0; j < colLen; j++) {
				currChar = this.MSA.get(i).get(j);
				this.charactersSet.add(currChar);
			}
		}

		// Napraviti mapu pojavljivanja karaktera bez '-'(za brze clearanje LDCounte		ra)
		for(Character tmpChar : this.charactersSet) {
			if(tmpChar != '-') {
				this.lettersFreqBlueprint.put(tmpChar, 0);
			}
		}
	}

	public void calculateProbabilities() {
		int HMMIndex = 1;
		double denominatorTmp;

		LDCounter transLDCounter = new LDCounter();
		LDCounter transNextLDCounter = new LDCounter();
		LDCounter emitLDCounter = new LDCounter();

		Map<String, Double> tmpTransMap;
		Map<Character, Double> tmpEmitMap;

		/*
		Pripremiti sva moguca stanja - de fakto HMM = mapa tranzicijskih i emisijskih vjerojatnosti
		m = broj konzerviranih regija
		Start stanje, End stanje
		m Deletion stanja(D1 do Dm)
		m Mimatch stanja(M1 do Mm)
		m+1 Insertion stanja(I0 do Im)
		 */

		// Popuniti mapu za emisije
		int m = this.conservedRegions.size();

		// ##############################				INIT				############################################
		// Emissions
		// Prvo Mimatch stanja(ima ih m)
		for(int i = 1; i <= m; i++) {
			tmpEmitMap = new HashMap<>();

			for(Character currChar : this.charactersSet) {
				tmpEmitMap.put(currChar, 0.);
			}

			this.emissionsMap.put("M" + i, tmpEmitMap);
		}

		// Emissions
		// Zatim Insertion stanja(ima ih m+1, tj. indeks im krece od 0)
		for(int i = 0; i <= m; i++) {
			tmpEmitMap = new HashMap<>();

			for(Character currChar : this.charactersSet) {
				tmpEmitMap.put(currChar, 0.);
			}

			this.emissionsMap.put("I" + i, tmpEmitMap);
		}

		// Transitions
		// Start i I0
		tmpTransMap = new HashMap<>();
		tmpTransMap.put("M1", 0.);
		tmpTransMap.put("D1", 0.);
		tmpTransMap.put("I0", 0.);
		this.transitionsMap.put("S0", tmpTransMap);

		// I0 sada
		tmpTransMap = new HashMap<>(tmpTransMap);
		this.transitionsMap.put("I0", tmpTransMap);

		for(int i = 1; i < m; i++) {
			// Di -> D_i+1, Di -> Ii, Di -> M_i+1
			tmpTransMap = new HashMap<>();
			tmpTransMap.put("D" + (i+1), 0.);
			tmpTransMap.put("I" + i, 0.);
			tmpTransMap.put("M" + (i+1), 0.);
			this.transitionsMap.put("D" + i, tmpTransMap);

			// Mi -> M_i+1, Mi -> Ii, Mi -> D_i+1
			tmpTransMap = new HashMap<>(tmpTransMap);
			this.transitionsMap.put("M" + i, tmpTransMap);

			// Ii -> Ii, Ii -> D_i+1, Ii -> M_i+1
			tmpTransMap = new HashMap<>(tmpTransMap);
			this.transitionsMap.put("I" + i, tmpTransMap);
		}

		// End i Im - handliranje zadnjeg retka
		// Dm -> E, Dm -> Im ; Mm -> E, Mm -> Im ; Im -> Im, Im -> E
		tmpTransMap = new HashMap<>();
		tmpTransMap.put("E", 0.);
		tmpTransMap.put("I" + m, 0.);
		this.transitionsMap.put("D" + m, tmpTransMap);

		tmpTransMap = new HashMap<>(tmpTransMap);
		this.transitionsMap.put("M" + m, tmpTransMap);

		tmpTransMap = new HashMap<>(tmpTransMap);
		this.transitionsMap.put("I" + m, tmpTransMap);

		// ##############################				INIT				############################################

		// ##############################			GLAVNI DIO - PROBS CALC			####################################

		boolean inConservedRegion;
		boolean nextColumnConservedRegion;
		char currChar;

		// Pocetak(stanje S) handlirati rucno - 0. stupac
		inConservedRegion = conservedRegions.contains(0);

		// Prebrojati znakove u nultom stupcu
		transLDCounter.countAtIndex(0);

		// Izracunati vjerojatnosti za S i I(0. stupac sekvence)
		// S -> I0, S -> M1 i S -> D1
		// I0 -> I0, I0 -> D1 i I0 ->M1
		if(inConservedRegion) {
			// Bijela zona, racunamo vjerojatnosti za D1 i M1(za I0 pseudocountovi su vec stavljeni)

			// Tranzicije
			denominatorTmp = (this.numOfRows + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR);
			double transitionToMimatchState = ((double)transLDCounter.letterCount + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
			double transitionToDeleteState = ((double)transLDCounter.dashCount + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
			double transitionToInsertionState = this.PSEUDOCOUNT_TRANSITION/denominatorTmp;

			tmpTransMap = new HashMap<>();
			tmpTransMap.put("M1", transitionToMimatchState);
			tmpTransMap.put("D1", transitionToDeleteState);
			tmpTransMap.put("I0", transitionToInsertionState);

			this.transitionsMap.put("S0", tmpTransMap);

			// Match(osim njega i Insertion) moze emitirati znakove. Ali S ne emitira znakove pa nista tu
		} else {
			// Siva zona - racunamo vjerojatnosti za I0
			// Usli smo u sivu regiju. Pogledajmo na kojem indexu je prva konzervativna regija
			int nextConsrvRegIndex = this.conservedRegions.get(0);

			// Pogledati koliko sekvenci unutar sive regije ima barem 1 slovo - to su tranzicije S -> I
			// Ukoliko nema nijednoga slova - pogledati koji je prvi znak u bijeloj zoni
			// Ukoliko je to '-' onda je to tranzicija S -> D, a inace S -> M
			int start2Insertion = 0, start2Deletion = 0, start2Mimatch = 0;
			int insertion2Insertion = 0, insertion2Mimatch = 0, insertion2Deletion = 0;

			for(int currRow = 0; currRow < this.numOfRows; currRow++) {
				boolean ideInsertion = false;

				for(int currCol = 0; currCol < nextConsrvRegIndex; currCol++) {
					currChar = MSA.get(currRow).get(currCol);

					// Provjeravamo Start -> Insertion?
					if(currChar != '-') {
						ideInsertion = true;

						// Pobrojati ovaj znak koji se pojavljuje u I0 stanju
						emitLDCounter.addLetter(currChar);

						// Insertion -> Insertion sub-for
						// Hoce li ponovo otici u Insertion - i koliko puta? Usput potrebno brojati emitirane charove
						for(++currCol; currCol < nextConsrvRegIndex; currCol++) {
							currChar = MSA.get(currRow).get(currCol);

							if(currChar != '-') {
								insertion2Insertion++;

								// Pobrojati ovaj znak koji se pojavljuje u I0 stanju
								emitLDCounter.addLetter(currChar);
							}
						}

						break;
					}
				}

				currChar = MSA.get(currRow).get(nextConsrvRegIndex);
				if(ideInsertion) {
					start2Insertion++;

					// Konacno pogledati kamo ce otici poslije svih tih insertion loopova?
					if(currChar != '-') {
						insertion2Mimatch++;
					} else {
						insertion2Deletion++;
					}
				} else {
					// Provjeriti jesmo li otisli u Mimatch ili Deletion
					if(currChar != '-') {
						start2Mimatch++;
					} else {
						start2Deletion++;
					}
				}
			}

			// Izracunati vjerojatnosti tranzicije, staviti ih u mapu, i tu mapu staviti u mapu :D
			denominatorTmp = (this.numOfRows + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR);
			double transitionToMimatchState = ((double)start2Mimatch + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
			double transitionToDeleteState = ((double)start2Deletion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
			double transitionToInsertionState = ((double)start2Insertion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;

			tmpTransMap = new HashMap<>();
			tmpTransMap.put("M1", transitionToMimatchState);
			tmpTransMap.put("D1", transitionToDeleteState);
			tmpTransMap.put("I0", transitionToInsertionState);

			// To su bile vjerojatnosti tranzicije za stanje S
			this.transitionsMap.put("S0", tmpTransMap);

			// Sanse za I0 -> I0, D1 ili M1
			// Koliko je uslo u I0 stanje, toliko mora i izaci + loopanje unutar samoga I0 stanja
			// denominatorTmp = number of total transitions outgoing from the Insertion state
			denominatorTmp = start2Insertion + insertion2Insertion + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR;
			transitionToMimatchState = ((double)insertion2Mimatch + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
			transitionToDeleteState = ((double)insertion2Deletion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
			transitionToInsertionState = ((double)insertion2Insertion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;

			tmpTransMap = new HashMap<>();
			tmpTransMap.put("M1", transitionToMimatchState);
			tmpTransMap.put("D1", transitionToDeleteState);
			tmpTransMap.put("I0", transitionToInsertionState);
			this.transitionsMap.put("I0", tmpTransMap);

			// I0 moze emitirati i znakove, pa treba i to obraditi za emisijske koeficijente
			tmpEmitMap = new HashMap<>();
			denominatorTmp = emitLDCounter.letterCount + this.PSEUDOCOUNT_EMISSION_DENOMINATOR;
			for(Map.Entry<Character, Integer> currEntry : emitLDCounter.letterFreqs.entrySet()) {
				Character currSubChar = currEntry.getKey();
				Integer currSubCharNum = currEntry.getValue();

				tmpEmitMap.put(currSubChar, ((double)currSubCharNum + this.PSEUDOCOUNT_EMISSION)/denominatorTmp);
			}
			this.emissionsMap.put("I0", tmpEmitMap);
		}

		// Sada obradivati ostatak stanja - krenuti ponovo od PRVE bijele regije
		for(int currCol = this.conservedRegions.get(0); currCol < this.numOfCols; currCol++) {
			// Na pocetku - ocistiti countere i mape
			emitLDCounter.clear();
			transNextLDCounter.clear();
			transLDCounter.clear();

			// Jesmo li dosli pred kraj? - je li prosli stupac bio zadnja bijela regija? Jesmo li sada na zadnjem stupcu?
			if((HMMIndex+1) == this.conservedRegions.size()) {
				// Za zadnjega treba racunati D -> E, M -> E, D -> I, E -> I;
				// dodatno, treba racunati I -> I, I -> E
				// Znaci, sve vezano uz End
				this.endCalc(transLDCounter, transNextLDCounter, emitLDCounter, currCol);
				return;
			}

			// Prebrojati znakove u stupcu
			transLDCounter.countAtIndex(currCol);

			/*
			Automatski smo u bijeloj zoni - zato sto na pocetku algoitma se preskoci siva zona.
			Kasnije u algoritmu, ukoliko poslije bijele zone slijedi siva, onda se ta siva zona isto preskoci,
			i algoritam nastavlja dalje sa bijelom zonom.

			Tako da zapravo jedino trebamo provjeravati je li slijedeca zona bijela ili siva. I naravno na pocetku
			algoritma, prilikom obrade S i I0 stanja, treba provjeriti je li 0. stupac bijela ili siva zona
			 */
			// Bijela zona, racunamo vjerojatnosti za Dx i Mx, ali i za Ix
			// Trebamo izbrojati u sljedecem stupcu stavke
			nextColumnConservedRegion = (currCol+1) == conservedRegions.get(HMMIndex);

			double transitionToMimatchState, transitionToDeleteState, transitionToInsertionState;

			if(nextColumnConservedRegion) {
				// Iduca bijela -  B->B
				// M -> M ili M -> D
				// Broj M stanja - znaci letterCount
				// Ako ide u M, onda u sljedecem stupcu moramo gleadti letterCount
				// A ako ide u D, onda u sljedecem stupcu moramo gledati dashCount
				transNextLDCounter.countAtIndex(currCol+1);

				denominatorTmp = (transLDCounter.letterCount + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR);
				transitionToMimatchState = ((double)transNextLDCounter.letterCount + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToDeleteState = ((double)transNextLDCounter.dashCount + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToInsertionState = (this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;

				tmpTransMap = new HashMap<>();
				tmpTransMap.put("M" + (HMMIndex+1), transitionToMimatchState);
				tmpTransMap.put("D" + (HMMIndex+1), transitionToDeleteState);
				tmpTransMap.put("I" + HMMIndex, transitionToInsertionState);
				this.transitionsMap.put("M" + HMMIndex, tmpTransMap);

				// D -> M ili D -> D
				denominatorTmp = (transLDCounter.dashCount + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR);
				transitionToMimatchState = ((double)transNextLDCounter.letterCount + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToDeleteState = ((double)transNextLDCounter.dashCount + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToInsertionState = (this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;

				tmpTransMap = new HashMap<>();
				tmpTransMap.put("M" + (HMMIndex+1), transitionToMimatchState);
				tmpTransMap.put("D" + (HMMIndex+1), transitionToDeleteState);
				tmpTransMap.put("I" + HMMIndex, transitionToInsertionState);
				this.transitionsMap.put("D" + HMMIndex, tmpTransMap);

				// Emisije za Mimatch stanje
				tmpEmitMap = new HashMap<>();
				denominatorTmp = transLDCounter.letterCount + this.PSEUDOCOUNT_EMISSION_DENOMINATOR;
				for(Map.Entry<Character, Integer> currEntry : transLDCounter.letterFreqs.entrySet()) {
					Character currSubChar = currEntry.getKey();
					Integer currSubCharNum = currEntry.getValue();

					tmpEmitMap.put(currSubChar, ((double)currSubCharNum + this.PSEUDOCOUNT_EMISSION)/denominatorTmp);
				}
				this.emissionsMap.put("M" + HMMIndex, tmpEmitMap);
			} else {
				// Iduca siva - B->S -- na kraju treba preskociti sivu zonu na indexu sekvence
				// M -> I(AC-C) ili negdje kasnije(unutar sive npr. --A) moze ici u M -> I, a tek u sljedecoj bijeloj ide M -> M i M -> D
				// M -> I, ali treba gledati i D -> I
				int nextConsrvRegIndex = this.conservedRegions.get(HMMIndex);
				transNextLDCounter.countAtIndex(nextConsrvRegIndex);

				int mimatch2Insertion = 0, mimatch2Mimatch = 0, mimatch2Deletion = 0;
				int deletion2Insertion = 0, deletion2Mimatch = 0, deletion2Deletion = 0;
				int insertion2Insertion = 0, insertion2Mimatch  = 0, insertion2Deletion = 0;

				for(int currRow = 0; currRow < this.numOfRows; currRow++) {
					boolean ideInsertion = false;

					for(int currSubCol = currCol + 1; currSubCol < nextConsrvRegIndex; currSubCol++) {
						currChar = MSA.get(currRow).get(currSubCol);
						if(currChar != '-') {
							// Ako naide na bilo koji znak, znaci ide u Ix
							ideInsertion = true;
							emitLDCounter.addLetter(currChar);

							// Moramo brojati Ins -> Ins
							for(++currSubCol; currSubCol < nextConsrvRegIndex; currSubCol++) {
								currChar = MSA.get(currRow).get(currSubCol);
								if(currChar != '-') {
									insertion2Insertion++;
									emitLDCounter.addLetter(currChar);
								}
							}

							break;
						}
					}

					currChar = MSA.get(currRow).get(currCol);
					if(ideInsertion) {
						// Iz M je otisao u I
						if(currChar != '-') {
							mimatch2Insertion++;
						} else {
							deletion2Insertion++;
						}

						// Otisli smo u Ix - moramo gledati gdje cemo dalje iz Ix ici - gledamo sljedeci bijeli znak
						currChar = this.MSA.get(currRow).get(nextConsrvRegIndex);
						if(currChar != '-') {
							insertion2Mimatch++;
						} else {
							insertion2Deletion++;
						}
					} else {
						// Nije bilo insertiona, pa samo gledamo sta dolazi poslije sive regije
						// M -> M, M -> D, te D -> M i D -> D
						if(currChar != '-') {
							// Bili smo u M - sad gledamo sljedeci bijeli, tj. gdje cemo otici
							currChar = MSA.get(currRow).get(nextConsrvRegIndex);
							if(currChar != '-') {
								// M -> M
								mimatch2Mimatch++;
							} else {
								// M -> D
								mimatch2Deletion++;
							}
						} else {
							// Bili smo u D - sad gledamo sljedeci bijeli, tj. gdje cemo otici
							currChar = MSA.get(currRow).get(nextConsrvRegIndex);
							if(currChar != '-') {
								// Otisli smo u M -- D -> M
								deletion2Mimatch++;
							} else {
								// Otisli smo u D -- D -> D
								deletion2Deletion++;
							}
						}

					}
				}

				// Poslije svih redaka mozemo konacno izracunati vjerojatnosti
				// M -> M, M -> D, i M -> I
				// Bili smo u Bijeloj regiji, broj M je letterCount u nazivniku
				denominatorTmp = transLDCounter.letterCount + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR;
				transitionToDeleteState = ((double)mimatch2Deletion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToMimatchState = ((double)mimatch2Mimatch + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToInsertionState = ((double)mimatch2Insertion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;

				tmpTransMap = new HashMap<>();
				tmpTransMap.put("D" + (HMMIndex+1), transitionToDeleteState);
				tmpTransMap.put("M" + (HMMIndex+1), transitionToMimatchState);
				tmpTransMap.put("I" + HMMIndex, transitionToInsertionState);
				this.transitionsMap.put("M" + HMMIndex, tmpTransMap);

				// D -> M, D -> D i D -> I
				denominatorTmp = transLDCounter.dashCount + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR;
				transitionToDeleteState = ((double)deletion2Deletion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToMimatchState = ((double)deletion2Mimatch + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToInsertionState = ((double)deletion2Insertion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;

				tmpTransMap = new HashMap<>();
				tmpTransMap.put("D" + (HMMIndex+1), transitionToDeleteState);
				tmpTransMap.put("M" + (HMMIndex+1), transitionToMimatchState);
				tmpTransMap.put("I" + HMMIndex, transitionToInsertionState);
				this.transitionsMap.put("D" + HMMIndex, tmpTransMap);

				// Ix -> Ix, Ix -> D_x+1, Ix -> I_x+1
				// denominatorTmp = zbroj(sveukupno) odlazaka iz Insertiona
				denominatorTmp = mimatch2Insertion + deletion2Insertion + insertion2Insertion + this.PSEUDOCOUNT_TRANSITION_DENOMINATOR;
				transitionToMimatchState = ((double)insertion2Mimatch + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToDeleteState = ((double)insertion2Deletion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;
				transitionToInsertionState = ((double)insertion2Insertion + this.PSEUDOCOUNT_TRANSITION)/denominatorTmp;

				tmpTransMap = new HashMap<>();
				tmpTransMap.put("D" + (HMMIndex+1), transitionToDeleteState);
				tmpTransMap.put("M" + (HMMIndex+1), transitionToMimatchState);
				tmpTransMap.put("I" + HMMIndex, transitionToInsertionState);
				this.transitionsMap.put("I" + HMMIndex, tmpTransMap);

				// Emisije za M stanje
				tmpEmitMap = new HashMap<>();
				denominatorTmp = transLDCounter.letterCount + this.PSEUDOCOUNT_EMISSION_DENOMINATOR;
				for(Map.Entry<Character, Integer> currEntry : transLDCounter.letterFreqs.entrySet()) {
					Character currSubChar = currEntry.getKey();
					Integer currSubCharNum = currEntry.getValue();

					tmpEmitMap.put(currSubChar, ((double)currSubCharNum + this.PSEUDOCOUNT_EMISSION)/denominatorTmp);
				}
				this.emissionsMap.put("M" + HMMIndex, tmpEmitMap);

				// Vrijeme za transmisije u I stanju
				tmpEmitMap = new HashMap<>();
				denominatorTmp = emitLDCounter.letterCount + this.PSEUDOCOUNT_EMISSION_DENOMINATOR;
				for(Map.Entry<Character, Integer> currEntry : emitLDCounter.letterFreqs.entrySet()) {
					Character currSubChar = currEntry.getKey();
					Integer currSubCharNum = currEntry.getValue();

					tmpEmitMap.put(currSubChar, ((double)currSubCharNum + this.PSEUDOCOUNT_EMISSION)/denominatorTmp);
				}
				this.emissionsMap.put("I" + HMMIndex, tmpEmitMap);

				// Preskociti sivu regiju, pomaknuti HMMindex
				currCol = nextConsrvRegIndex-1;
			}

			/*
			Posto preskacemo sive regije(te sukladno tome pomicemo i currCol index, to znaci da kada god dodemo ovdje
			smo obradili 1 bijelu zonu, dakle povecavamo HMMIndex koji defakto prati trenutan indeks DIM stanja,
			tj. trenutan indeks u HMM-u(za razliku od currCol koji prati trenutan indeks u sekvencama).
			 */
			HMMIndex++;
		}
	}

	public void endCalc(LDCounter transLDCounter, LDCounter transNextLDCounter, LDCounter emitLDCounter,
						int currCol) {
		int HMMIndex = this.conservedRegions.size();
		Map<String, Double> tmpTransMap;
		Map<Character, Double> tmpEmitMap;

		// Clear counters
		transLDCounter.clear();
		transNextLDCounter.clear();
		emitLDCounter.clear();

		// Sada mozemo krenuti racunati tu zadnju regiju - nalazimo se u sekvencama na zadnjoj bijeloj regiji
		transLDCounter.countAtIndex(currCol);

		// M -> E, D -> E ; NO, moguce su i tranzicije M -> I, D -> I
		// Nadalje, tranzicije za I: I -> I, I -> E
		int mimatch2End = 0, mimatch2Insertion = 0, deletion2End = 0, deletion2Insertion = 0;
		int insertion2Insertion = 0, insertion2End = 0;
		char currChar;

		for(int currSubRow = 0; currSubRow < this.numOfRows; currSubRow++) {
			for(int currSubCol = currCol+1; currSubCol < this.numOfCols; currSubCol++) {
				// Provjeriti hocemo li ici u Insertion?
				currChar = this.MSA.get(currSubRow).get(currSubCol);

				if(currChar != '-') {
					// Otisli smo u Insertion - treba provjeriti jesmo li otisli u I iz M ili D
					// Potrebno pobrojati emitiran znak u Im stanju
					emitLDCounter.addLetter(currChar);

					currChar = this.MSA.get(currSubRow).get(currCol);
					if(currChar != '-') {
						mimatch2Insertion++;
					} else {
						deletion2Insertion++;
					}

					// Potrebno je brojati koliko loopova se bude desilo prije Enda: Im -> Im
					for(++currSubCol; currSubCol < this.numOfCols; currSubCol++) {
						currChar = this.MSA.get(currSubRow).get(currSubCol);

						if(currChar != '-') {
							emitLDCounter.addLetter(currChar);
							insertion2Insertion++;
						}
					}

					// Izasli smo iz sive regije u nista, tj. na kraj
					break;
				}
			}
		}

		// Sada treba izracunati vjerojatnosti
		// Prvo Dm -> Im, i D -> E
		double denominatorTmp = 2 * this.PSEUDOCOUNT_TRANSITION;			// Mnozimo s 2 jer postoje 2 vjerojatnosti za 2 strelice
		double transition2Insertion, transition2End;

		transition2Insertion = ((double)deletion2Insertion + this.PSEUDOCOUNT_TRANSITION)/(transLDCounter.dashCount + denominatorTmp);
		transition2End = 1 - transition2Insertion;

		tmpTransMap = new HashMap<>();
		tmpTransMap.put("I" + HMMIndex, transition2Insertion);
		tmpTransMap.put("E", transition2End);
		this.transitionsMap.put("D" + HMMIndex, tmpTransMap);

		// Sada ide Mm -> Im, i Mm -> E
		tmpTransMap = new HashMap<>();
		transition2Insertion = ((double)mimatch2Insertion + this.PSEUDOCOUNT_TRANSITION)/(transLDCounter.letterCount + denominatorTmp);
		transition2End = 1 - transition2Insertion;

		tmpTransMap.put("I" + HMMIndex, transition2Insertion);
		tmpTransMap.put("E", transition2End);
		this.transitionsMap.put("M" + HMMIndex, tmpTransMap);

		// Sada treba izracunati vjerojatnosti za Im -> Im, i Im -> E
		double insertionUkupnoOut = mimatch2Insertion + deletion2Insertion + insertion2Insertion + denominatorTmp;
		transition2Insertion = ((double)insertion2Insertion + this.PSEUDOCOUNT_TRANSITION)/insertionUkupnoOut;
		transition2End = 1 - transition2Insertion;

		tmpTransMap = new HashMap<>();
		tmpTransMap.put("I" + HMMIndex, transition2Insertion);
		tmpTransMap.put("E", transition2End);
		this.transitionsMap.put("I" + HMMIndex, tmpTransMap);

		// Sanse emisije za M i I stanja
		// Prvo za M emisije
		tmpEmitMap = new HashMap<>();
		denominatorTmp = transLDCounter.letterCount + this.PSEUDOCOUNT_EMISSION_DENOMINATOR;
		for(Map.Entry<Character, Integer> currEntry : transLDCounter.letterFreqs.entrySet()) {
			Character currSubChar = currEntry.getKey();
			Integer currSubCharNum = currEntry.getValue();

			tmpEmitMap.put(currSubChar, ((double)currSubCharNum + this.PSEUDOCOUNT_EMISSION)/denominatorTmp);
		}
		this.emissionsMap.put("M" + HMMIndex, tmpEmitMap);

		// Vrijeme za I emisije
		tmpEmitMap = new HashMap<>();
		denominatorTmp = emitLDCounter.letterCount + this.PSEUDOCOUNT_EMISSION_DENOMINATOR;
		for(Map.Entry<Character, Integer> currEntry : emitLDCounter.letterFreqs.entrySet()) {
			Character currSubChar = currEntry.getKey();
			Integer currSubCharNum = currEntry.getValue();

			tmpEmitMap.put(currSubChar, ((double)currSubCharNum + this.PSEUDOCOUNT_EMISSION)/denominatorTmp);
		}
		this.emissionsMap.put("I" + HMMIndex, tmpEmitMap);
	}

	public void updateHMMProfile() {
		// TODO: micanje stupaca sa svim '-' ovdje mozda?
		this.numOfRows = this.MSA.size();
		this.numOfCols = this.MSA.get(0).size();
		this.makeCharsSet();
		this.filterConservedRegions();
	}

	private void filterConservedRegions() {
		this.conservedRegions = new ArrayList<>();

    	HashMap<Character, Integer> map = new HashMap<>();
    	for(int j = 0; j < numOfCols; j++) {
    		for(int i = 0; i < numOfRows; i++) {
				Character key = MSA.get(i).get(j);
				map.merge(key, 1, (alt, neu) -> alt+1);
    		}
    		
    		// Provjerit dal stupac pripada u conserved region
    		for (Map.Entry<Character, Integer>  currEntry : map.entrySet()) {
				Character key = currEntry.getKey();
    			Integer value = currEntry.getValue();

    			if(((key != '-') && (double) value/numOfRows > THRESHOLD)) {
					// Pripada
					this.conservedRegions.add(j);
					break;
				}
    		}
    		map.clear();
    	}
	}

	String MSA2String() {
		StringJoiner sj = new StringJoiner(";\r\n");

		// Strings themselves
		for(var currInputLine : this.MSA) {
			sj.add(currInputLine.toString());
		}

		// Conserved region stati
		StringJoiner indicesSJ = new StringJoiner("  ", "{", "}");
		int conservCol = this.conservedRegions.get(0);
		boolean currColConserved;
		for(int colIndex = 0, conservIndex = 1; colIndex < this.numOfCols-1; colIndex++) {
			currColConserved = (conservCol == colIndex);
			if(currColConserved) {
				conservCol = this.conservedRegions.get(conservIndex++);
				indicesSJ.add("*");
			} else {
				indicesSJ.add("-");
			}
		}

		conservCol = this.conservedRegions.get(this.conservedRegions.size()-1);
		if((this.numOfCols-1) == conservCol) {
			indicesSJ.add("*");
		} else {
			indicesSJ.add("-");
		}

		sj.add(indicesSJ.toString());
		return sj.toString();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ProfileHMM{\r\n");
		sb.append("id=").append(this.id);
		sb.append(", numOfCols=").append(this.numOfCols);
		sb.append(", numOfRows=").append(this.numOfRows);
		sb.append(", threshold=").append(this.THRESHOLD);
		sb.append(", PSEUDOCOUNT_EMISSION=").append(this.PSEUDOCOUNT_EMISSION);
		sb.append(", PSEUDOCOUNT_TRANSITION=").append(this.PSEUDOCOUNT_TRANSITION);
		sb.append(", PSEUDOCOUNT_EMISSION=").append(this.PSEUDOCOUNT_EMISSION_DENOMINATOR);
		sb.append(", PSEUDOCOUNT_TRANSITION=").append(this.PSEUDOCOUNT_TRANSITION_DENOMINATOR);
		sb.append("\r\nMSA=\r\n").append(this.MSA2String());
		sb.append("\r\nconservedRegions=").append(this.conservedRegions);
		sb.append("\r\ncharactersSet=").append(this.charactersSet);
		sb.append("\r\ntransitionsMap=").append(this.transitionsMap);
		sb.append("\r\nemissionsMap=").append(this.emissionsMap);
		sb.append('}');
		return sb.toString();
	}
}
