import java.util.LinkedList;
import java.util.List;

public class MSAAlgorithm {

    LinkedList<LinkedList<Character>> MSA;
    ProfileHMM profileHMM;
    Viterbi viterbi;
    List<String> toAlignSequences;

    public MSAAlgorithm(LinkedList<LinkedList<Character>> startingMSA, List<String> toAlignSequences) {
        this.MSA = startingMSA;
        this.toAlignSequences = toAlignSequences;
        this.profileHMM = new ProfileHMM(this.MSA, 0.01, 0.01, 0.1);
        viterbi = new Viterbi(this.profileHMM);
    }

    private void initHMMAndViterbi() {
        this.profileHMM.updateHMMProfile();
        this.profileHMM.calculateProbabilities();
        this.viterbi.fillLogMaps();
        System.out.println(this.profileHMM);
    }

    public void runAlgorithm() {
        System.out.println("...Running algorithm...");

        // First construct profile HMM - calculate probs
        this.initHMMAndViterbi();

        // Now - progressive algorithm
        String viterbiTmp;
//        int counter = 0;

        for(String toAlignSeq : this.toAlignSequences) {
//            if(counter > 0) {
//                break;
//            }
//            counter++;

            viterbiTmp = this.viterbi.getViterbiPath(toAlignSeq);
            System.out.println(toAlignSeq);
            System.out.println("Viterbi output: ");
            System.out.println(viterbiTmp);
            System.out.println("#####################################################################################");

            // Sada kada imamo Viterbi output, treba poravnati Viterbi sa MSA, novu sekvencu dodati u MSA
            this.align(toAlignSeq, this.MSA, viterbiTmp, profileHMM.conservedRegions);

            // Sad kad smo poravnali, treba konstruirati novi profil HMM
            this.initHMMAndViterbi();
        }

        System.out.println("MSA nakon viterbi aligna: ");
        System.out.println(MSA2String(this.MSA));
    }

    public static String MSA2String(LinkedList<LinkedList<Character>> MSA) {
        StringBuilder sb = new StringBuilder();
        for(var seqTmp : MSA) {
            for(Character currChar : seqTmp) {
                sb.append(currChar);
            }
            sb.append("\r\n");
        }
        return sb.toString();
    }

    public void align(String line, LinkedList<LinkedList<Character>> MSA
            , String pattern, List<Integer> conservedRegions) {
        StringBuilder newLine = new StringBuilder();

        pattern = pattern.substring(1);             // Remove 'S' from the beginning
        int letterIndex = 0;                           // Counts the current index in pattern and line

        /*
        Moramo pratiti na kojem se trenutno indeksu nalazimo u msa kako bismo mogli provjeriti sive zone. Znaci,
        ovaj indeks povecavamo cijelo vrijeme. No, kada ga koristimo, koristimo njegovu verziju uvecanu za
        insertionOffset.

        De fakto stoga je msaIndex = patternIndex + insertionOffset
\         */
        int msaIndex = 0;
        int insertionsOffset = 0;
        //int hmmIndex = 0;                           // Counts the current index in the HMM
        int patternLen = pattern.length()-1;
        int msaLen = MSA.getFirst().size();

        int nextConservRegIndex = 0, currConservRegIndex = -1;
        int nextConservRegNum, conservRegNum;
        boolean inConservedRegion;
        char currChar;

        for(int patternIndex = 0; patternIndex < patternLen; patternIndex++) {
            msaIndex = newLine.length();
            /*
            Prvo provjeriti je li trenutan indeks siva ili bijela zona? Ako je siva zona onda cemo preskociti do
            slijedece bijele. Tako da na pocetku petlje treba povecati indeks!
             */
            if(nextConservRegIndex == conservedRegions.size()) {
                /*
                Nema vise bijelih regija, tako da smo definitivno u sivoj jer jos uvijek nizmo izasli van MSA!
                 */
                inConservedRegion = true;
                nextConservRegNum = msaLen + insertionsOffset;     // Namjerno stavimo ovo da je poslije kraja
            } else {
                // Provjeriti jesmo li u bijeloj ili sivoj
//                conservRegNum = conservedRegions.get(currConservRegIndex) + insertionsOffset;
                nextConservRegNum = conservedRegions.get(nextConservRegIndex) + insertionsOffset;
                inConservedRegion = (msaIndex == nextConservRegNum);
            }

            // Dohvatiti komandu
            char command = pattern.charAt(patternIndex);

            // E sada, ovisno o regiji i trenutnoj komandi radimo drugacije akcije
            if(!inConservedRegion) {
                /*
                U ovoj regiji - siva zona - moramo prvo provjeriti je li komanda I. Ako komanda nije I, onda mozemo do
                kraja sive regije staviti crtice i dalje nastaviti sa svojim poslom. Na kraju ne treba continue - nego
                fallamo through na sljedeci dio petlje koji ce obraditi non-I komandu

                Ako komanda je I, onda stavljamo svoje znakove u sivu regiju. Do kraja sive regije popunjavamo crticama.
                Ukoliko je siva regija kraca od broja nasih insertiona, moramo gore umetnuti crtice. Na kraju continue,
                kako bi se pokupila slijedeca komanda.
                 */
                if(command == 'I') {
                    /*
                    Trebati ce pobrojati broj nasih Insertiona nasuprot broju sivih regija u MSA
                    3 slucaja:
                        1) Siva regija duza od broja nasih insertiona
                        2) Jednako su dugi - super
                        3) Siva regija kraca od broja nasih insertiona
                     */
                    int greyLen = nextConservRegNum - msaIndex;

                    // Brojanje insertiona - look ahead
                    int patternIndexTmp = patternIndex;
                    char commandTmp = 'I';
                    int insLen = 0;
                    while(commandTmp == 'I') {
                        insLen++;
                        commandTmp = pattern.charAt(++patternIndexTmp);
                    }

                    // Trebamo obraditi 3 slucaja
                    int diff = greyLen - insLen;
                    if(diff >= 0) {
                        /*
                        Tu obradujemo 2 slucaja:
                        Siva regija je duza - super -> znaci staviti nasa slova i sve crtice onda
                        Jednako duge - isto super - znaci stavimo samo nasa slova, nema potreba za crticama
                         */
                        // Prvo stavljamo znakove
                        for(int i = 0; i < insLen; i++) {
                            currChar = line.charAt(letterIndex++);
                            newLine.append(currChar);
                            patternIndex++;     // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
                        }

                        // Do kraja popuniti crticama
                        newLine.append("-".repeat(diff));

                        // Na kraju pomaknuti indeks, continuati kako bi se radila slijedeca komanda poslije svih Ins-eva
                    } else {
                        /*
                        Siva regija je kraca - morati cemo iznad u MSA dodavati '-'e.
                        Trebalo bi crtice dodati na kraj sive regije. Dakle, zelimo prvo izracunati
                         */
                        // Idemo dodati nasa slova
                        for(int i = 0; i < greyLen; i++) {
                            newLine.append(line.charAt(letterIndex++));
                            patternIndex++;     // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
                        }

                        /*
                        Sad kad smo dodali slova koliko stane u sivu regiju, moramo dodati ostatak slova u nas string,
                        a crtice u sekvence iznad
                         */
                        diff *= -1;
                        for(int i = 0; i < diff; i++) {
                            newLine.append(line.charAt(letterIndex++));

                            for(var seq : MSA) {
                                seq.add(patternIndex, '-');
                            }

                            insertionsOffset++;     // Ovo povecavamo ovdje zato sto se MSA produzava
                            patternIndex++;     // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
                        }

                        // Sva slova i crtice dodane, mozemo continuati i ici dalje sa sljedecom komandom
                    }

                    // Tu ide continue, jer zelimo da petlja dohvati i obradi i sljedecu komandu
                    // Treba pomaknuti indeks na slijedecu komandu
                    patternIndex--;
                    continue;
                } else {
                    /*
                    Do kraja sive regije staviti crtice, a onda nastaviti dalje normalno sa HMM-om.
                     */
                    for(int subMSAIndex = msaIndex; subMSAIndex < nextConservRegNum; subMSAIndex++) {
                        newLine.append('-');
                    }
                    // Tu ne ide continue - jer zelimo da se trenutna komanda obradi
                    nextConservRegIndex++;
                }
            } else {
                /*
                Bijela zona
                Ako je bio Insertion, moramo dodati taj znak, a u ostale sekvence dodati '-' - i tako za sve insertione.
                Prvo izbrojati broj Insertiona u patternu, advanceati indeks za to
                Zatim dalje nastaviti sa komandama
                 */
                if(command == 'I') {
                    // Brojanje insertiona - look ahead
                    int patternIndexTmp = patternIndex;
                    char commandTmp = 'I';
                    int insLen = 0;
                    while(commandTmp == 'I') {
                        insLen++;
                        commandTmp = pattern.charAt(++patternIndexTmp);
                    }

                    for(int i = 0; i < insLen; i++) {
                        currChar = line.charAt(letterIndex++);
                        newLine.append(currChar);

                        // Treba dodati u sve ostale sekvence - na ta podrucja
                        for(var seq : MSA) {
                            seq.add(patternIndex, '-');
                        }

                        insertionsOffset++;     // Ovo povecavamo ovdje zato sto se MSA produzava
                        patternIndex++;     // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
                    }

                    // Smanjiti indeks i continue
                    patternIndex--;
                    continue;
                } else {
                    /*
                    Ako nije bio Insertion, onda samo idemo dalje na procesiranje M ili D
                    U bijeloj smo zoni, i pritome nismo u I(znaci ne radimo novu sivu zonu). Dakle mozemo advanceati
                    indeks bijelih zona - jer je slijedeca bijela zona
                     */
//                    currConservRegIndex++;
                    nextConservRegIndex++;
                }
            }

            // Sada slijedi onaj lagani dio - gdje djelujemo ovisno radili se o D ili M
            switch (command) {
                case 'M' -> newLine.append(line.charAt(letterIndex++));
                case 'D' -> newLine.append('-');
            }
        }

        // Pretvoriti u listu karaktera i dodati u MSA
        LinkedList<Character> lastRow = Viterbi.toLinkedList(newLine.toString());
        MSA.add(lastRow);
    }

}
