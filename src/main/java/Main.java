import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class Main {
	
	static List<String> input = new ArrayList<String>();
    static String inputFile = "ToAlignSequences.multifasta";
    static String startingMSAFile = "StartingMSA.multifasta";
    static Map<String, String> multiFastaInput;

    public static void main(String[] args) throws IOException {
        System.out.println("HMM Progressive Alignment Greetings!");

        // N - how many sequences will be located in the alignment
        // M - max sequence length
        // Ovo mozda kasnije - optimizacija lista
//        int N, M;
//        try(Scanner sc = new Scanner(System.in)) {
//            System.out.println("Number of sequences in MSA >");
//            N = sc.nextInt();
//
//            System.out.println("Max sequence length >");
//            M = sc.nextInt();
//        }

        // First we have to read the starting alignment upon which the profile HMM will be built
        LinkedList<LinkedList<Character>> MSA = new LinkedList<>();
        Main.multiFastaInput = MultiFastaReader.readSeqsFile(Main.startingMSAFile);
        for(var val : Main.multiFastaInput.values()) {
            MSA.add(Viterbi.toLinkedList(val));
        }

        // Read the rest of the sequences that yet need to be aligned
        Main.multiFastaInput = MultiFastaReader.readSeqsFile(Main.inputFile);
        List<String> toAlignSeqs = new ArrayList<>(Main.multiFastaInput.values());

        System.out.println("Starting MSA: ");
        for(var currItem : MSA) {
            System.out.println(currItem);
        }
        System.out.println("#########################################################################################");

        System.out.println("Sequences to be aligned: ");
        for(String currItem : toAlignSeqs) {
            System.out.println(currItem);
        }

        // Based on this starting MSA a ProfileHMM will be construced
        MSAAlgorithm msaAlgo = new MSAAlgorithm(MSA, toAlignSeqs);
        msaAlgo.runAlgorithm();
    }

    public static void testFillInputData() {
        input.add("ACGAT-AACG");
        input.add("AG-AT-ATCG");
        input.add("TGCAT-ATCG");
        input.add("AG-TAAATCG");
        input.add("AG-AT-AT-G");
    }

    public static class MultiFastaReader {
        private static boolean startOfFile = true;
        private static String currId = null;
        private static StringBuilder sequenceBuffer;
        private static Map<String, String> multiFastaMap;

        public static Map<String, String> readSeqsFile(String file) throws IOException {
//            MultiFastaReader.startOfFile = true;
//            MultiFastaReader.currIndex = -1;

            // Read a multi-fasta file here
            MultiFastaReader.multiFastaMap = new HashMap<>();
            Path multiFastaFile = Paths.get(file);
            BufferedReader br = new BufferedReader(new FileReader(multiFastaFile.toFile()));

            // Manually read the first line
            String st = br.readLine();
            String[] parts = st.split(" ", 2);
            MultiFastaReader.currId = parts[0];
            MultiFastaReader.sequenceBuffer = new StringBuilder();

//            while ((st = br.readLine()) != null) {
//                readStreamLine(st);
//            }
            br.lines().forEach(MultiFastaReader::readStreamLine);

            // Add the last sequence
            MultiFastaReader.multiFastaMap.put(MultiFastaReader.currId, MultiFastaReader.sequenceBuffer.toString());

            return MultiFastaReader.multiFastaMap;
        }

        private static void readStreamLine(String line) {
            if(line.startsWith(">")) {
                String[] parts = line.split(" ", 2);

                if(!parts[0].equals(MultiFastaReader.currId)) {
                    // Put former Sequence into map
                    MultiFastaReader.multiFastaMap.put(MultiFastaReader.currId, MultiFastaReader.sequenceBuffer.toString());

                    // Prepare for new Sequence
                    MultiFastaReader.currId = parts[0];
                    MultiFastaReader.sequenceBuffer = new StringBuilder();
                }
            } else {
                sequenceBuffer.append(line.strip());
            }
        }
    }

    //    public static void sandboxFunction() throws FileNotFoundException {
//        //Main.multiFastaInput = MultiFastaReader.readInputFile();
//
//        testFillInputData();
//
//        ProfileHMM profileHMM = new ProfileHMM(input, 0.01, 0.01, 0.7);
//        profileHMM.calculateProbabilities();
//        System.out.println(profileHMM);
//
//        Viterbi vb = new Viterbi(profileHMM);
//        System.out.println(vb.getViterbiPath("ACCTGCA"));
//    }
}
