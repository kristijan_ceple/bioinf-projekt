
import java.util.*;

public class Viterbi {

	public enum VGState {
		D(0),
		I(1),
		M(2),
		S(3),
		E(4)
		;

		/**
		 * Underlying state integer representation
		 */
		private final int stateCode;

		VGState(int stateCode) {
			this.stateCode = stateCode;
		}

		public static VGState int2VGState(int stateCode) {
			return switch (stateCode) {
				case 0 -> VGState.D;
				case 1 -> VGState.I;
				case 2 -> VGState.M;
				default -> throw new IllegalArgumentException("VGState return not implemented for this number!");
			};
		}
	}

	public class VGStateCell {
		private VGState cellState = VGState.S;
		private int stateIndex = -1;
		private double cellVal = 0;

		public VGStateCell(VGState cellState, int stateIndex, double cellVal) {
			this.cellState = cellState;
			this.stateIndex = stateIndex;
			this.cellVal = cellVal;
		}

		@Override
		public String toString() {
			return this.cellState.name() + this.stateIndex;
		}
	}

	Map<String, Map<String,Double>> logTransitionsProbs = new HashMap<>();
	Map<String, Map<Character, Double>> logEmissionsProb = new HashMap<>();
	/**
	 * "S" + sequence itself in order to simplify the Viterbi Graph construction
	 */
	LinkedList<Character> sequence;			// The Sequence that will be aligned to the ProfileHMM MSA
	VGStateCell[][] VGTable;
	ProfileHMM profileHMM;
	
	public Viterbi(ProfileHMM profileHMM) {
        this.profileHMM = profileHMM;
		this.fillLogMaps();
    }

    public String getViterbiPath(String sequence) {
		this.sequence = Viterbi.toLinkedList("S" + sequence);
		return this.correctViterbiGraph();
	}

	public static LinkedList<Character> toLinkedList(String sequence) {
		LinkedList<Character> list = new LinkedList<>();
		for(int i = 0; i < sequence.length(); i++) {
			list.add(sequence.charAt(i));
		}
		return list;
	}

	private String correctViterbiGraph () {
		int seqLen = this.sequence.size();
		int m = this.profileHMM.conservedRegions.size();
		//this.table = new double[m][seqLen];
		// Pripremiti tablicu proslih stanja - za sada znamo prvi redak i stupac
		this.VGTable = new VGStateCell[m+1][seqLen];

		/*
		Jako slicno algoritmu dinamickog programiranja.
		Pomicanje udesno -> Insertion
		Pomicanje dijagonalno -> Match
		Pomicanje prema dolje -> Deletion
		Potrebno doci u skroz donji desni kut(End stanje), krecemo iz gornjeg desnog(Start stanje)
		Prilikom popunjavanja tablice, moramo voditi brige o trenutnim indeksima. Paziti na to da I ima loopove pa ide
		sam u sebe, a D i M uvijek napreduju trenutan HMM indeks.
		*/

		// 1. U Start stanju smo. Idem popuniti prvi redak i prvi stupac sa brojkama
		// Start inicijaliziramo sa kumulativnom vrijednoscu 0
		this.VGTable[0][0] = new VGStateCell(VGState.S, 0, 0);

		double tmpVal, fromUp, fromLeft, fromDiagonal;
		double tmpTransVal;
		char currChar;
		Map<Character, Double> tmpEmitMap;

		// Popunimo S -> I0, tj. table[0][1]
		currChar = this.sequence.get(1);
		tmpVal = 0 + this.logTransitionsProbs.get("S0").get("I0") + this.logEmissionsProb.get("I0").get(currChar);
		this.VGTable[0][1] = new VGStateCell(VGState.I, 0, tmpVal);

		// Popunjavamo prvi redak
		tmpTransVal = this.logTransitionsProbs.get("I0").get("I0");
		tmpEmitMap = this.logEmissionsProb.get("I0");
		for(int i = 2; i < seqLen; i++) {
			// Gledamo samo ulijevo, I0 -> I0
			currChar = this.sequence.get(i);
			tmpVal = this.VGTable[0][i-1].cellVal + tmpTransVal + tmpEmitMap.get(currChar);
			this.VGTable[0][i] = new VGStateCell(VGState.I, 0, tmpVal);
		}

		// Popunimo S -> D1, i zatim ostatak 1. stupca
		tmpVal = 0 + this.logTransitionsProbs.get("S0").get("D1") + 0;
		this.VGTable[1][0] = new VGStateCell(VGState.D, 1, tmpVal);

		// Sada ostatak 1. stupca - D1 -> D2, D2 -> D3, itd...
		for(int i = 2; i <= m; i++) {
			tmpVal = this.VGTable[i-1][0].cellVal + this.logTransitionsProbs.get("D" + (i-1)).get("D" + i);
			this.VGTable[i][0] = new VGStateCell(VGState.D, i, tmpVal);
		}

		// Sada ostatak tablice, North-West -> South-East
		VGStateCell prevState;
		VGState maxState;
		double maxVal;
		int prevStateIndexLeft, prevStateIndexUp, prevStateIndexDiagonal, stateIndex;
		for(int currVGRow = 1; currVGRow <= m; currVGRow++) {
			for(int currVGCol = 1; currVGCol < seqLen; currVGCol++) {
				currChar = this.sequence.get(currVGCol);
				/*
				Sada, idemo popunjavati, mozemo doci iz tri smjera:
					fromUp - dosli od gore, znaci prev_state -> D
					fromLeft - dosli od lijevo, znaci prev_state -> I
					fromDiagonal - dijagonalno, znaci prev_state -> M

					Pri tome treba pratiti indekse za D, I i M. Kako cemo znati koje je
					previous stanje? Trebalo bi nekako i njega(njih) pratiti.

					Treba znati stanja iz retka iznad i trenutnog retka. Indeksi stanja su ovisni o trenutnoj poziciji u
					sekvenci???
				 */
				// Prvo od gore - deletion
				prevState = this.VGTable[currVGRow-1][currVGCol];
				prevStateIndexUp = prevState.stateIndex;
				fromUp = this.VGTable[currVGRow-1][currVGCol].cellVal
						+ this.logTransitionsProbs.get(prevState.toString()).get("D" + (prevStateIndexUp+1));

				// Zatim gledajmo od lijevo - insertion
				prevState = this.VGTable[currVGRow][currVGCol-1];
				prevStateIndexLeft = prevState.stateIndex;
				fromLeft = this.VGTable[currVGRow][currVGCol-1].cellVal
						+ this.logTransitionsProbs.get(prevState.toString()).get("I" + prevStateIndexLeft)
						+ this.logEmissionsProb.get("I" + prevState.stateIndex).get(currChar);

				// Konacno gledajmo dijagonalno
				prevState = this.VGTable[currVGRow-1][currVGCol-1];
				prevStateIndexDiagonal = prevState.stateIndex;
				fromDiagonal = this.VGTable[currVGRow-1][currVGCol-1].cellVal
						+ this.logTransitionsProbs.get(prevState.toString()).get("M" + (prevStateIndexDiagonal+1))
						+ this.logEmissionsProb.get("M" + (prevState.stateIndex+1)).get(currChar);

				// Sada treba naci max i staviti ga u tablicu
				maxState = VGState.D;
				maxVal = fromUp;
				stateIndex = prevStateIndexUp+1;

				if(fromLeft > maxVal) {
					maxVal = fromLeft;
					maxState = VGState.I;
					stateIndex = prevStateIndexLeft;
				}

				if(fromDiagonal > maxVal) {
					maxVal = fromDiagonal;
					maxState = VGState.M;
					stateIndex = prevStateIndexDiagonal+1;
				}

				// Staviti najvecega u tablicu
				this.VGTable[currVGRow][currVGCol] = new VGStateCell(maxState, stateIndex, maxVal);

				// Nastaviti dalje i tako ici dok ne popunimo tablicu
			}
		}

		// Na kraju ce uvijek otici u End! Vrijeme za traceback
		return this.correctViterbiTraceback();
	}

	private String correctViterbiTraceback() {
		StringBuilder path = new StringBuilder("E");
		int m = this.profileHMM.conservedRegions.size();
		int rowsNum = m+1;
		int colsNum = this.sequence.size();

		int currRow = rowsNum-1;
		int currCol = colsNum-1;

		// Start from the South-Eastern corner, go to Start state
		VGStateCell currCell;
		VGState currCellState;

		while(true) {
			currCell = this.VGTable[currRow][currCol];
			currCellState = currCell.cellState;

			path.append(currCellState.name());
			switch(currCellState) {
				case D:
					// Dosli od gore
					currRow--;
					break;
				case I:
					// Dosli od lijeva
					currCol--;
					break;
				case M:
					// Dosli od dijagonalno
					currRow--;
					currCol--;
					break;
				case S:
					// Gotovo! Samo treba obrnuti put tako da ide od S prema E
					return path.reverse().toString();
			}
		}
	}

    /**
     * Logarithm base 2 is used!
     */
    public void fillLogMaps() {
		Objects.requireNonNull(this.profileHMM);

        // First logTransitionsProbs
        this.profileHMM.transitionsMap.entrySet().stream()
                .forEach(stringMapEntry -> {
                    String state = stringMapEntry.getKey();
                    Map<String, Double> oldTransitions = stringMapEntry.getValue();
                    Map<String, Double> newTransitions = new HashMap<>();

                    oldTransitions.entrySet().stream()
                            .forEach(stringDoubleEntry -> {
                                String destination = stringDoubleEntry.getKey();
                                Double prob = stringDoubleEntry.getValue();
                                prob = Math.log1p(prob);
                                newTransitions.put(destination, prob);
                            });

                    this.logTransitionsProbs.put(state, newTransitions);
                });

        // Then logEmissionProbs
        this.profileHMM.emissionsMap.entrySet().stream()
                .forEach(stringMapEntry -> {
                    String state = stringMapEntry.getKey();
                    Map<Character, Double> oldEmissions = stringMapEntry.getValue();
                    Map<Character, Double> newEmissions = new HashMap<>();

                    oldEmissions.entrySet().stream()
                            .forEach(characterDoubleEntry -> {
                                Character currChar = characterDoubleEntry.getKey();
                                Double prob = characterDoubleEntry.getValue();

                                prob = Math.log1p(prob);
                                newEmissions.put(currChar, prob);
                            });
                    this.logEmissionsProb.put(state, newEmissions);
                });
    }
}

// Some leftover code in I think traceback???
//		// Calculate 3 values, see where we came from
//		// From up - Deletion
//		fromUp = this.VGTable[currRow-1][currCol].cellVal
//				+ this.logTransitionsProbs.get().get(currCell.toString())
//				+ this.logEmissionsProb.get().get();
//
//		// From left - insertion
//		fromLeft = this.VGTable[currRow][currCol-1].cellVal
//				+ this.logTransitionsProbs.get().get()
//				+ this.logEmissionsProb.get().get();
//
//		// From diagonal - MiMatch
//		fromDiagonal = this.VGTable[currRow-1][currCol-1].cellVal
//				+ this.logTransitionsProbs.get().get()
//				+ this.logEmissionsProb.get().get();